
RFCs Supported by Cyrus IMAP
****************************

The following is an inventory of RFCs supported by Cyrus IMAP.

**RFC 822**

   Standard for the format of ARPA Internet text messages, obsoleted
   by **RFC 2822**.

**RFC 0977**

   Network News Transfer Protocol

**RFC 1036**

   Standard for interchange of USENET messages

**RFC 1176**

   Interactive Mail Access Protocol: Version 2

**RFC 1342**

   Representation of Non-ASCII Text in Internet Message Headers

**RFC 1652**

   SMTP Service Extension for 8bit-MIMEtransport

**RFC 1730**

   Internet Message Access Protocol - version 4, obsoleted by **RFC
   2060**, **RFC 2061**, **RFC 3501**.

**RFC 1869**

   SMTP Service Extensions

**RFC 1870**

   SMTP Service Extension for Message Size Declaration

**RFC 1939**

   Post Office Protocol - Version 3 (POP3)

**RFC 1951**

   DEFLATE Compressed Data Format Specification version 1.3

**RFC 2033**

   Local Mail Transfer Protocol

**RFC 2034**

   SMTP Service Extension for Returning Enhanced Error Codes

**RFC 2045**

   Multipurpose Internet Mail Extensions (MIME) Part One: Format of
   Internet Message Bodies

**RFC 2046**

   Multipurpose Internet Mail Extensions (MIME) Part Two: Media Types

**RFC 2047**

   MIME (Multipurpose Internet Mail Extensions) Part Three: Message
   Header Extensions for Non-ASCII Text

**RFC 2060**

   Internet Message Access Protocol - Version 4rev1, obsoleted by
   **RFC 3501**.

**RFC 2086**

   IMAP4 ACL Extension, obsoleted by **RFC 4314**.

   Note: Backwards compatibility with this RFC is to be obsoleted.

**RFC 2087**

   IMAP4 QUOTA extension

**RFC 2088**

   IMAP4 non-synchronizing literals

**RFC 2177**

   IMAP4 IDLE command

**RFC 2192**

   IMAP URL Scheme, obsoleted by **RFC 5092**.

**RFC 2193**

   IMAP4 Mailbox Referrals

**RFC 2195**

   IMAP/POP AUTHorize Extension for Simple Challenge/Response

**RFC 2246**

   The TLS Protocol Version 1.0

**RFC 2298**

   Extensible Message Format for Message Disposition Notifications
   (MDNs)

**RFC 2342**

   IMAP4 Namespace

**RFC 2359**

   IMAP4 UIDPLUS extension, obsoleted by **RFC 4315**

**RFC 2444**

   The One-Time-Password SASL Mechanism

**RFC 2449**

   POP3 Extension Mechanism

**RFC 2518**

   HTTP Extensions for Distributed Authoring -- WEBDAV

**RFC 2595**

   Using TLS with IMAP, POP3 and ACAP

**RFC 2617**

   HTTP Authentication: Basic and Digest Access Authentication

**RFC 2817**

   HTTP Upgrading to TLS Within HTTP/1.1

**RFC 2818**

   HTTP Over TLS

**RFC 2821**

   Simple Mail Transfer Protocol

**RFC 2822**

   Internet Message Format

**RFC 2831**

   Using Digest Authentication as a SASL Mechanism

**RFC 2920**

   SMTP Service Extension for Command Pipelining

**RFC 2971**

   IMAP4 ID extension

**RFC 2980**

   Common NNTP Extensions

**RFC 3028**

   Sieve: A Mail Filtering Language

**RFC 3206**

   The SYS and AUTH POP Response Codes

**RFC 3207**

   SMTP Service Extension for Secure SMTP over TLS

**RFC 3253**

   Versioning Extensions to WebDAV (Web Distributed Authoring and
   Versioning)

**RFC 3339**

   Date and Time on the Internet: Timestamps

**RFC 3348**

   IMAP4 Child Mailbox Extension

**RFC 3431**

   Sieve Extension: Relational Tests

**RFC 3463**

   Enhanced Mail System Status Codes

**RFC 3501**

   Internet Message Access Protocol - version 4rev1

**RFC 3502**

   IMAP MULTIAPPEND extension

**RFC 3516**

   IMAP4 Binary Content Extension

**RFC 3598**

   Sieve Email Filtering -- Subaddress Extension, obsoleted by **RFC
   5233**.

**RFC 3656**

   MUPDATE Protocol (For Cyrus Murder)

**RFC 3691**

   Internet Message Access Protocol (IMAP) UNSELECT command

**RFC 3744**

   Web Distributed Authoring and Versioning (WebDAV) Access Control
   Protocol

**RFC 3834**

   Recommendations for Automatic Responses to Electronic Mail

**RFC 3848**

   ESMTP and LMTP Transmission Types Registration

**RFC 3894**

   Sieve Extension: Copying Without Side Effects

**RFC 3977**

   Network News Transfer Protocol (NNTP)

**RFC 4287**

   The Atom Syndication Format

**RFC 4314**

   IMAP4 Access Control List (ACL) Extension

**RFC 4315**

   Internet Message Access Protocol (IMAP) - UIDPLUS extension

**RFC 4331**

   Quota and Size Properties for Distributed Authoring and Versioning
   (DAV) Collections

**RFC 4346**

   The Transport Layer Security (TLS) Protocol Version 1.1

**RFC 4422**

   Simple Authentication and Security Layer (SASL)

**RFC 4466**

   Collected Extensions to IMAP4 ABNF

**RFC 4467**

   Internet Message Access Protocol (IMAP) - URLAUTH Extension,
   updated by **RFC 5092**.

**RFC 4469**

   Internet Message Access Protocol (IMAP) CATENATE Extension

**RFC 4505**

   Anonymous Simple Authentication and Security Layer (SASL) Mechanism

**RFC 4550**

   Internet Email to Support Diverse Service Environments (Lemonade)
   Profile

**RFC 4551**

   IMAP Extension for Conditional STORE Operation or Quick Flag
   Changes Resynchronization

**RFC 4559**

   SPNEGO-based Kerberos and NTLM HTTP Authentication in Microsoft
   Windows

**RFC 4616**

   The PLAIN Simple Authentication and Security Layer (SASL) Mechanism

**RFC 4642**

   Using Transport Layer Security (TLS) with Network News Transfer
   Protocol (NNTP)

**RFC 4643**

   Network News Transfer Protocol (NNTP) Extension for Authentication

**RFC 4644**

   Network News Transfer Protocol (NNTP) Extension for Streaming Feeds

**RFC 4731**

   IMAP4 Extension to SEARCH Command for Controlling What Kind of
   Information Is Returned

**RFC 4791**

   Calendaring Extensions to WebDAV (CalDAV)

**RFC 4918**

   HTTP Extensions for Web Distributed Authoring and Versioning
   (WebDAV)

**RFC 4954**

   SMTP Service Extension for Authentication

**RFC 4959**

   IMAP Extension for Simple Authentication and Security Layer (SASL)
   Initial Client Response

**RFC 4978**

   The IMAP COMPRESS Extension

**RFC 5032**

   WITHIN Search Extension to the IMAP Protocol

**RFC 5034**

   The Post Office Protocol (POP3) Simple Authentication and Security
   Layer (SASL) Authentication Mechanism

**RFC 5092**

   IMAP URL Scheme, updated by **RFC 5593**.

**RFC 5161**

   The IMAP ENABLE Extension

**RFC 5162**

   IMAP4 Extensions for Quick Mailbox Resynchronization

**RFC 5173**

   Sieve Email Filtering: Body Extension

**RFC 5228**

   Sieve: A Mail Filtering Language

**RFC 5229**

   Sieve Email Filtering: Variables Extension

**RFC 5230**

   Sieve Email Filtering: Vacation Extension

**RFC 5231**

   Sieve Email Filtering: Relational Extension

**RFC 5232**

   Sieve Email Filtering: Imap4flags Extension

   New in version 2.5.0.

**RFC 5233**

   Sieve Email Filtering: Subaddress Extension

**RFC 5256**

   Internet Message Access Protocol - SORT and THREAD Extensions

**RFC 5257**

   Internet Message Access Protocol - ANNOTATE Extension

**RFC 5258**

   Internet Message Access Protocol version 4 - LIST Command
   Extensions

**RFC 5260**

   Sieve Email Filtering: Date and Index Extensions

   New in version 2.5.0.

**RFC 5321**

   Simple Mail Transfer Protocol

**RFC 5322**

   Internet Message Format

**RFC 5397**

   WebDAV Current Principal Extension

**RFC 5423**

   Internet Message Store Events

**RFC 5429**

   Sieve Email Filtering: Reject and Extended Reject Extensions

   Note: Only the "reject" action is currently implemented.

**RFC 5435**

   Sieve Email Filtering: Extension for Notifications

**RFC 5436**

   Sieve Notification Mechanism: mailto

**RFC 5464**

   The IMAP METADATA Extension

**RFC 5465**

   The IMAP NOTIFY Extension

**RFC 5524**

   Extended URLFETCH for Binary and Converted Parts

**RFC 5536**

   Netnews Article Format

**RFC 5537**

   Netnews Architecture and Protocols

**RFC 5545**

   Internet Calendaring and Scheduling Core Object Specification
   (iCalendar)

**RFC 5546**

   iCalendar Transport-Independent Interoperability Protocol (iTIP)

**RFC 5593**

   Internet Message Access Protocol (IMAP) - URL Access Identifier
   Extension

**RFC 5689**

   Extended MKCOL for Web Distributed Authoring and Versioning
   (WebDAV)

**RFC 5804**

   A protocol for Remotely Managing Sieve Scripts

**RFC 5819**

   IMAP4 Extension for Returning STATUS Information in Extended LIST

**RFC 5957**

   Display-Based Address Sorting for the IMAP4 SORT Extension

**RFC 5995**

   Using POST to Add Members to Web Distributed Authoring and
   Versioning (WebDAV) Collections

**RFC 6047**

   iCalendar Message-Based Interoperability Protocol (iMIP)

**RFC 6101**

   The Secure Sockets Layer (SSL) Protocol Version 3.0

   Note: SSLv3 is considered inscure as it is vulnerable to
     POODLE.Support for SSLv3 is being deprecated and removed.

**RFC 6131**

   Sieve Vacation Extension: "Seconds" Parameter

**RFC 5463**

   Sieve Email Filtering: Ihave Extension

**RFC 6154**

   IMAP LIST Extension for Special-Use Mailboxes

**RFC 6321**

   xCal: The XML Format for iCalendar

**RFC 6350**

   vCard Format Specification

**RFC 6352**

   CardDAV: vCard Extensions to Web Distributed Authoring and
   Versioning (WebDAV)

**RFC 6376**

   DomainKeys Identified Mail (DKIM) Signatures

**RFC 6578**

   Collection Synchronization for Web Distributed Authoring and
   Versioning (WebDAV)

**RFC 6585**

   Additional HTTP Status Codes

**RFC 6609**

   Sieve Email Filtering: Include Extension

**RFC 6638**

   Scheduling Extensions to CalDAV

**RFC 6764**

   Locating Services for Calendaring Extensions to WebDAV (CalDAV) and
   vCard Extensions to WebDAV (CardDAV)

**RFC 6797**

   HTTP Strict Transport Security (HSTS)

**RFC 6851**

   Internet Message Access Protocol (IMAP) - MOVE Extension

   New in version 2.5.0.

**RFC 7230**

   Hypertext Transfer Protocol (HTTP/1.1): Message Syntax and Routing

**RFC 7231**

   Hypertext Transfer Protocol (HTTP/1.1): Semantics and Content

**RFC 7232**

   Hypertext Transfer Protocol (HTTP/1.1): Conditional Requests

**RFC 7233**

   Hypertext Transfer Protocol (HTTP/1.1): Range Requests

**RFC 7234**

   Hypertext Transfer Protocol (HTTP/1.1): Caching

**RFC 7235**

   Hypertext Transfer Protocol (HTTP/1.1): Authentication

**RFC 7238**

   The Hypertext Transfer Protocol Status Code 308 (Permanent
   Redirect)

**RFC 7239**

   Forwarded HTTP Extension

**RFC 7240**

   Prefer Header for HTTP

**RFC 7265**

   jCal: The JSON Format for iCalendar

**RFC 7529**

   Non-Gregorian Recurrence Rules in the Internet Calendaring and
   Scheduling Core Object Specification (iCalendar) (obsoletes draft-
   ietf-calext-rscale)

**RFC 7808**

   Time Zone Data Distribution Service

**RFC 7809**

   CalDAV: Time Zones by Reference

**RFC 7953**

   Calendar Availability


IETF RFC Drafts
===============

draft-murchison-lmtp-ignorequota

   LMTP Service Extension for Ignoring Recipient Quotas

[MS-NTHT]   NTLM Over HTTP Protocol Specification

draft-ietf-sieve-regex

   Sieve Email Filtering -- Regular Expression Extension

draft-martin-sieve-notify

   Sieve -- An extension for providing instant notifications

draft-york-vpoll

   VPOLL: Consensus Scheduling Component for iCalendar

draft-desruisseaux-ischedule

   Internet Calendar Scheduling Protocol (iSchedule)

draft-thomson-hybi-http-timeout

   Hypertext Transfer Protocol (HTTP) Keep-Alive Header

draft-murchison-webdav-prefer

   Use of the Prefer Header Field in Web Distributed Authoring and
   Versioning (WebDAV)

draft-ietf-httpauth-basicauth-update

   The 'Basic' HTTP Authentication Scheme

draft-ietf-httpauth-digest

   HTTP Digest Access Authentication

draft-ietf-httpbis-auth-info

   The Hypertext Transfer Protocol (HTTP) Authentication-Info and
   Proxy- Authentication-Info Response Header Fields

draft-ietf-httpbis-cice

   Hypertext Transfer Protocol (HTTP) Client-Initiated Content-
   Encoding

   caldav-ctag     Calendar Collection Entity Tag (CTag) in CalDAV
   Brief Header    Microsoft 'Brief' header extension


RFC Wishlist
============

**RFC 5183**

   Sieve Email Filtering: Environment Extension

**RFC 5229**

   Sieve Email Filtering: Variables Extension

**RFC 5235**

   Sieve Email Filtering: Spamtest and Virustest Extensions

**RFC 5293**

   Sieve Email Filtering: Editheader Extension

**RFC 5429**

   Sieve Email Filtering: Reject and Extended Reject Extensions

**RFC 5437**

   Sieve Notification Mechanism: Extensible Messaging and Presence
   Protocol (XMPP)

**RFC 5490**

   The Sieve Mail-Filtering Language -- Extensions for Checking
   Mailbox Status and Accessing Mailbox Metadata

**RFC 5703**

   Sieve Email Filtering: MIME Part Tests, Iteration, Extraction,
   Replacement, and Enclosure

**RFC 6468**

   Sieve Notification Mechanism: SIP MESSAGE

**RFC 6558**

   Sieve Extension for Converting Messages before Delivery

**RFC 6785**

   Support for Internet Message Access Protocol (IMAP) Events in Sieve
